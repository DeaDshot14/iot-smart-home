# IoT- smart home

With this android app, one can control the electronic devices of your house. 
One has to first register and login on the app and also on the NodeMCU.
One can turn devices on/off, ask for status of a device, see the monthly statistics of units consumed.
One can also ask for current temperature and pressure.