package com.darren.developer.smarthome.model;

/**
 * Created by user on 31-03-2018.
 */

public class Device {
    private String dname;
    private boolean status;
    private int port;
    private int rating;

    public Device(){
    }

    public Device(String dname, boolean status, int port, int rating) {
        this.dname = dname;
        this.status = status;
        this.port = port;
        this.rating = rating;
    }

    public String getDname() {
        return dname;
    }

    public void setDname(String dname) {
        this.dname = dname;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
